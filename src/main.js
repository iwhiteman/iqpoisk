import { createApp } from 'vue';
import App from '@/App';

import '@/styles/clients/_demo.scss';
import '@/styles/clients/_1.scss';

if (process.env.NODE_ENV === 'development') {
  window.IQPoisk = {};
  window.IQPoisk.config = require('./app.config.json');
}

window.IQPoisk.searchParams = {
  iqpoisk: 'iqpoisk',
  mode: 'default/extended', // порядок важен
  query: 'query',
  sorting: 'sorting',
  category: 'category',
  price: 'price',
  page: 'page',
};

const app = document.createElement('div');
document.body.appendChild(app);
createApp(App).mount(app);
