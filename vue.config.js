const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: '',
  outputDir: 'build',
  productionSourceMap: false,
  css: {
    extract: false,
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/styles/_variables.scss";
          @import "@/styles/_mixins.scss";
          `,
      },
    },
  },
  configureWebpack: {
    optimization: {
      splitChunks: false,
    },
  },
});
